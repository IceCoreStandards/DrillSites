#!/usr/bin/python3

import typing
from bs4.element import Tag
import requests
from bs4 import BeautifulSoup
import os
import re
import sqlite3
import sys
import getopt


def strp(td):
    return td.text.strip()


def extract_icecore_locations(id, corelocation_h2):

    locations = []

    location = str(corelocation_h2.get("id")).strip()

    table_body = corelocation_h2.find_next("tbody")

    if table_body is not None:
        for tr in table_body.findChildren("tr"):
            tds = tr.findChildren("td")
            if len(tds) > 0:
                core = strp(tds[0])
                project = strp(tds[1])
                coords = strp(tds[2])
                lat = extract_coord(tds, "latitude")
                lon = extract_coord(tds, "longitude")
                # lon = tds[2].find('span', attrs={'class': 'longitude'})
                # if not lon == None:
                #     lon = strp(lon)

                altitude = strp(tds[3])
                drill_dates = strp(tds[4])
                depth = strp(tds[5])
                sources = strp(tds[6])

                p = re.compile(r"\[(.*?)\]")
                refs = re.findall(p, sources)

                citations, notes = extract_citations_and_notes(refs)

                row = [
                    id,
                    location,
                    core,
                    project,
                    lat,
                    lon,
                    altitude,
                    drill_dates,
                    depth,
                    citations,
                    notes,
                ]
                # print(row)
                print(".", end="", flush=True)
                locations.append(row)
                id += 1

    return locations


def convert_latlon_to_decimal(latlonstr):
    """Convert a latitude or longitude string to its floating point equivalent.

    Arguments:
        latlonstr {str} -- A latitude or longitude string like 0°9′S or 37°18′E.
                           Seconds (") are optional. Not None

    Returns:
        float -- The latitude or longitude in decimal notation
    """

    spl = latlonstr.split("°")

    degrees = float(spl[0])

    spl = spl[1].split("′")

    minutes = float(spl[0])

    seconds = 0
    if '"' in spl[1]:  # May not contain seconds
        spl = spl[1].split('"')
        seconds = float(spl[0])

    pos_or_neg = 1
    if "S" in spl[1] or "W" in spl[1]:
        pos_or_neg = -1

    return pos_or_neg * (degrees + (minutes / 60) + (seconds / 3600))


def extract_coord(tds, posclass):
    """Extract the latitude and longitude from a provided td element.

    Arguments:
        tds {TDElement} -- TD element containing latitude or longitude
        posclass {str} -- A string for the class -- either 'latitude' or 'longitude'

    Returns:
        float -- The latitude or longitude in decimal notation
    """
    latlonstr = tds[2].find("span", attrs={"class": posclass})
    if latlonstr is not None:
        latlonstr = strp(latlonstr)
        return convert_latlon_to_decimal(latlonstr)
    else:
        return None


def extract_citations_and_notes(refs):
    citations = []
    notes = []
    if refs is not None:
        for r in refs:  # .groups():
            if "note " in r:
                notes.append(int(r.split("note ")[1]))
            else:
                citations.append(int(r))
    return citations, notes


def delete_db(db_name: str):
    if os.path.exists(db_name):
        print(f"Deleting database {db_name}")
        os.remove(db_name)


def create_db(db_name: str, schemaname: str):

    conn = sqlite3.connect(db_name)

    cursor = conn.cursor()

    schemafile = open(schemaname, "r")

    schema = schemafile.read()
    schemafile.close()

    print(f'Creating db "{db_name}" from schema file "{schemaname}"')
    cursor.executescript(schema)
    conn.commit()
    conn.close()
    print("Tables created.")


def scrape_wiki(db_name: str):

    conn = sqlite3.connect(db_name)
    cursor = conn.cursor()

    ice_core_url = "https://en.wikipedia.org/wiki/List_of_ice_cores"

    page = requests.get(ice_core_url)

    if page.status_code != 200:
        print(
            f"Page request status code was {page.status_code}.  Expected a status code of 200.  Refusing to continue."
        )
        exit(-1)

    soup = BeautifulSoup(page.text, "html.parser")

    # * Find all the notes

    note_h2 = typing.cast(Tag, soup.find(id="Notes"))

    cite_notes = typing.cast(Tag, note_h2.find_next("ol")).findChildren(
        "li", attrs={"id": re.compile("cite_note.*")}
    )

    id = 1
    for cn in cite_notes:
        note = strp(cn.find("span", attrs={"class": "reference-text"}))
        cursor.execute("""INSERT INTO notes (id, note) VALUES (?,?)""", [id, note])

        # print(f'{id}, {note}')
        print(".", end="", flush=True)
        id = id + 1

    # * Find all of the citations (listed on the page as references)

    citations_h2 = typing.cast(Tag, soup.find(id="References"))

    citations = typing.cast(Tag, citations_h2.find_next("ol")).findChildren(
        "li", attrs={"id": re.compile("cite_note.*")}
    )

    id = 1
    for cn in citations:
        citation = strp(cn.find("span", attrs={"class": "reference-text"}))
        # print(f'{id}, {citation}')
        print(".", end="", flush=True)
        cursor.execute(
            """INSERT INTO citations (id, citation) VALUES (?,?)""", [id, citation]
        )
        id = id + 1

    # * Find all of the sources

    sources_h2 = typing.cast(Tag, soup.find(id="Sources"))

    sources = typing.cast(Tag, sources_h2.find_next("ul")).findChildren("li")

    id = 1
    for cn in sources:
        source = strp(cn.find("cite"))
        # print(f'{id}, {source}')
        print(".", end="", flush=True)
        cursor.execute(
            """INSERT INTO sources (id, source) VALUES (?,?)""", [id, source]
        )
        id = id + 1

    corelocation_h2 = typing.cast(
        Tag, soup.find("span", attrs={"class": "mw-headline"})
    )

    # * Try to find the sources for each citation

    p = re.compile(r"(.*?)\s+.*\((.*?)\)")

    for c in cursor.execute("""SELECT id, citation FROM citations""").fetchall():
        nameAndyear = re.findall(p, c[1])

        if len(nameAndyear) == 1 and len(nameAndyear[0]) == 2:
            cursor.execute(
                """INSERT INTO citations_sources 
                                SELECT ?, sources.id 
                                FROM sources 
                                WHERE source LIKE ?""",
                [c[0], f"%{nameAndyear[0][0]}%({nameAndyear[0][1]})%"],
            )

    # * Find all of the ice cores

    icecores = []
    id = 1
    while str(corelocation_h2.get("id")).strip() != "See_also":
        found_cores = extract_icecore_locations(id, corelocation_h2)
        id += len(found_cores)
        icecores.extend(found_cores)
        corelocation_h2 = typing.cast(
            Tag, corelocation_h2.find_next("span", attrs={"class": "mw-headline"})
        )

    cursor.executemany(
        """INSERT INTO icecores VALUES (?,?,?,?,?,?,?,?,?)""",
        [i[0:9] for i in icecores],
    )

    for i in icecores:
        cursor.executemany(
            """INSERT INTO icecores_citations VALUES (?,?)""", [[i[0], c] for c in i[9]]
        )
        cursor.executemany(
            """INSERT INTO icecores_notes VALUES (?,?)""", [[i[0], n] for n in i[10]]
        )

    conn.commit()
    conn.close()


def usage():
    print("./icecorescraper.py <option>")
    print(
        """options:
    -h --help   : print this usage message
    -c --create : delete and create a new sql database"""
    )


if __name__ == "__main__":

    db_name = "icecores.db"
    schemaname = "icecores-schema.sql"

    try:
        opts, args = getopt.getopt(sys.argv[1:], "hc", ["create"])
    except getopt.GetoptError as err:
        # print help information and exit:
        print(err)  # will print something like "option -a not recognized"
        usage()
        sys.exit(2)

    if len(opts) == 0:
        usage()
        sys.exit(2)

    for o, a in opts:
        if o in ["-c", "--create"]:
            delete_db(db_name)
            create_db(db_name, schemaname)
            scrape_wiki(db_name)
        if o in ["-h", "--help"]:
            usage()

    print(
        f"\nComplete. The list of ice cores has been placed in the following sqlite file: {os.path.abspath(db_name)}"
    )

# cspell:ignore strp tbody