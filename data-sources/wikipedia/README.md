#  List of ice core sites extracted from wikipedia page

## Description

The ./icecorescraper.py program extracts ice-core information from the URL
https://en.wikipedia.org/wiki/List_of_ice_cores and generates the SQLite
database named ./icecores.db. The file ./icecores-schema.sql is used to create
the database tables in ./icecores.db. The CSV file is derived by dumping the
icecores table from ./icecores.db using SQLite's built-in .mode and .output
commands. For example,

```shell
$ sqlite3 ./icecores.db
sqlite> .headers on
sqlite> .mode csv
sqlite> .output icecores.csv
sqlite> SELECT * FROM icecores;
sqlite> .quit
```

## Dependencies 
- Beautiful Soup 
- sqlite3

## Usage
./icecorescraper.py -h

## Author

Developed by Mark Royer
