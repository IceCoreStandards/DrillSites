# Data Source:
https://zenodo.org/record/7076469
Lindsey Davidge, Hanna L. Brooks, & Merlin L. Mah. (2022). Global ice drilling and archive location data for select ice cores (1.0.1) [Data set]. Zenodo. https://doi.org/10.5281/zenodo.7076469



## NOTES
1. Variations between local naming and data conventions result in data gaps. Data is provided as it was made available by each research institution.
2. LATITUDE and LONGITUDE for drill sites and ARCHIVE_LAT and ARCHIVE_LONG for included ice core repository locations are provided in decimal degrees.
3. BOTTOM_DEPTH, TOP_DEPTH, and SITE_ELEVATION are provided in meters, when available.
4. BOTTOM_AGE is provided in the format that it was made available by each research institution, when available. 
5. CORE_DIAMETER is provided in centimeters, when available.
6. Corresponding PUBLICATIONS are provided as they were made available by each research institution.
7. All core data were collected through personal correspondence with curators of research institution repositories, except for data from the NSF-ICF (retrieved from an online database) and U Copenhagen (inferred from https://nbi.ku.dk/).
8. EastGRIP ice core is listed with a total depth as reported by field team personnel on 08/08/2022; completed BOTTOM_DEPTH and END_DATE will differ.
9. Data from New Zealand, Australia, and Denmark is incomplete. Additional details about ice cores stored by these programs are available by direct contact with each facility.
10. Multiple cores from Australian sites DSSmain and DE08 have been collected; as of 2022, they were most recently updated to the surface in 2021 and 2018, respectively. 
