CREATE TABLE icecores (
  id INTEGER PRIMARY KEY,
  location TEXT NOT NULL,
  core TEXT NOT NULL,
  project TEXT NOT NULL,
  latitude TEXT,
  longitude TEXT,
  -- altitude in meters
  altitude TEXT,
  --  depth in meters
  drill_dates TEXT NOT NULL,
  depth TEXT NOT NULL
);
CREATE TABLE citations (
  id INTEGER PRIMARY KEY,
  citation TEXT NOT NULL
);
CREATE TABLE sources (
  id INTEGER PRIMARY KEY,
  source TEXT NOT NULL UNIQUE
);
CREATE TABLE notes (id INTEGER PRIMARY KEY, note TEXT NOT NULL);
-- * JOIN TABLES
CREATE TABLE icecores_citations(
  icecore_id INTEGER REFERENCES icecores(id),
  citation_id INTEGER REFERENCES refences(id),
  PRIMARY KEY (icecore_id, citation_id)
);
CREATE TABLE icecores_notes(
  icecore_id INTEGER REFERENCES icecores(id),
  note_id INTEGER REFERENCES notes(id),
  PRIMARY KEY (icecore_id, note_id)
);
CREATE TABLE citations_sources(
  citation_id INTEGER REFERENCES refences(id),
  source_id INTEGER REFERENCES sources(id),
  PRIMARY KEY (citation_id, source_id)
)
