# Ice Cores Database

"Law Dome boreholes list published in  Jong, Lenneke M., Christopher T. Plummer, Jason L. Roberts, Andrew D. Moy, Mark A. J. Curran, Tessa R. Vance, Joel B. Pedro, et al. 2022. “2000 Years of Annual Ice Core Data from Law Dome, East Antarctica.” Earth System Science Data 14 (7): 3313–28.
 https://doi.org/10.5194/essd-14-3313-2022."

## Last time updated Feb. 24, 2023

