﻿## **Data set title** 
Collection of ice core borehole locations and data sources

Project: NSF Summer 2019 workshop: Computing Arctic Data: Orono, ME - Spring 2019

Funded by NSF PLR--1848747 9/1/2018---8/31/2022

Submitted to ADC,  DOI: 
## **Abstract** 
Evaluating present and forecasting future climate trends in the Arctic is closely connected to understanding multidimensional paleoclimate data sets. In the Spring of 2019, a workshop was held to evaluate computer tools used by the US ice core community to access, compute and visualize publicly available data from the NSF Arctic Data Center (ADC). 

One of the workshop outcomes was a plan to develop comprehensive community driven data standards for reporting  ice core data. The first component to develop such standards is to use the same naming convention for ice cores. As a first step, the community started to develop a list of ice cores recovered globally by various research institutions, along with associated metadata. To date, we have summarized information on ice core  borehole locations collected as of February of 2023.  We will continue to include additional data sources and plan to create a summary list that captures information  on all recovered ice  cores in a single file that will follow evolving community standards [10.5281/](https://doi.org/10.5281/zenodo.7429501)[zenodo](https://doi.org/10.5281/zenodo.7429501)[.7429501](https://doi.org/10.5281/zenodo.7429501). 

## **Methodology**
With community input, a public gitlab repository was set up on February 15,  2021 to store tables with the information on ice core locations: [https](https://gitlab.com/IceCoreStandards/DrillSites/)[://](https://gitlab.com/IceCoreStandards/DrillSites/)[gitlab](https://gitlab.com/IceCoreStandards/DrillSites/)[.](https://gitlab.com/IceCoreStandards/DrillSites/)[com](https://gitlab.com/IceCoreStandards/DrillSites/)[/](https://gitlab.com/IceCoreStandards/DrillSites/)[IceCoreStandards](https://gitlab.com/IceCoreStandards/DrillSites/)[/](https://gitlab.com/IceCoreStandards/DrillSites/)[DrillSites](https://gitlab.com/IceCoreStandards/DrillSites/)[/](https://gitlab.com/IceCoreStandards/DrillSites/).

A number of  ice core metadata from different sources were collected and converted to CSV format.  Please contact the contacts listed  below for data sources if you have any specific questions on methodology or how data sets were collected. 


## **List of available datasets**

1) File Name: **BrazilianIceCores.csv**

Ice Core locations and metadata provided by Jefferson Cardia Simoes on February 22, 2023.

Contact information: 

Jefferson Cardia Simões

Vice-President, Scientific Committee on Antarctic Research (SCAR/ISC)

Centro Polar e Climático - CPC, Instituto de Geociências

Universidade Federal do Rio Grande do Sul - UFRGS

Av. Bento Gonçalves 9500

91501-970 - Porto Alegre, RS - BRASIL

Phone:  +55 51 3308-7327

E-mail:     jefferson.simoes@ufrgs.br

Homepage:  www.ufrgs.br/inctcriosfera

`                      `www.centropolar.com

1) File Name: **LawDome.csv**

Ice Core locations and metadata published in:

Jong, Lenneke M., Christopher T. Plummer, Jason L. Roberts, Andrew D. Moy, Mark A. J. Curran, Tessa R. Vance, Joel B. Pedro, et al. 2022. “2000 Years of Annual Ice Core Data from Law Dome, East Antarctica.” Earth System Science Data 14 (7): 3313–28.

[https](https://doi.org/10.5194/essd-14-3313-2022)[://](https://doi.org/10.5194/essd-14-3313-2022)[doi](https://doi.org/10.5194/essd-14-3313-2022)[.](https://doi.org/10.5194/essd-14-3313-2022)[org](https://doi.org/10.5194/essd-14-3313-2022)[/10.5194/](https://doi.org/10.5194/essd-14-3313-2022)[essd](https://doi.org/10.5194/essd-14-3313-2022)[-14-3313-2022](https://doi.org/10.5194/essd-14-3313-2022).


1) File Name: **ice\_archive\_data\_220913.csv**

Ice Core locations and metadata reported  in: 

Lindsey Davidge, Hanna L. Brooks, & Merlin L. Mah. (2022). Global ice drilling and archive location data for select ice cores (data set 1.0.1), published on  Zenodo. [https](https://doi.org/10.5281/zenodo.7076469)[://](https://doi.org/10.5281/zenodo.7076469)[doi](https://doi.org/10.5281/zenodo.7076469)[.](https://doi.org/10.5281/zenodo.7076469)[org](https://doi.org/10.5281/zenodo.7076469)[/10.5281/](https://doi.org/10.5281/zenodo.7076469)[zenodo](https://doi.org/10.5281/zenodo.7076469)[.7076469](https://doi.org/10.5281/zenodo.7076469)

Authors notes

Variations between local naming and data conventions result in data gaps. Data is provided as it was made available by each research institution.

LATITUDE and LONGITUDE for drill sites and ARCHIVE\_LAT and ARCHIVE\_LONG for included ice core repository locations are provided in decimal degrees.

BOTTOM\_DEPTH, TOP\_DEPTH, and SITE\_ELEVATION are provided in meters, when available.

BOTTOM\_AGE is provided in the format that it was made available by each research institution, when available.

CORE\_DIAMETER is provided in centimeters, when available.

Corresponding PUBLICATIONS are provided as they were made available by each research institution.

All core data were collected through personal correspondence with curators of research institution repositories, except for data from the NSF-ICF (retrieved from an online database) and U Copenhagen (inferred from https://nbi.ku.dk/).

EastGRIP ice core is listed with a total depth as reported by field team personnel on 08/08/2022; completed BOTTOM\_DEPTH and END\_DATE will differ.

Data from New Zealand, Australia, and Denmark is incomplete. Additional details about ice cores stored by these programs are available by direct contact with each facility.

Multiple cores from Australian sites DSSmain and DE08 have been collected; as of 2022, they were most recently updated to the surface in 2021 and 2018, respectively.

1) File Name: **IceCoresDatabase.csv**

Ice Core locations and metadata compiled in 2017 by Peter Neff

from multiple sources for Quantarctica

[https](https://www.npolar.no/en/quantarctica/#toggle-id-16)[://](https://www.npolar.no/en/quantarctica/#toggle-id-16)[www](https://www.npolar.no/en/quantarctica/#toggle-id-16)[.](https://www.npolar.no/en/quantarctica/#toggle-id-16)[npolar](https://www.npolar.no/en/quantarctica/#toggle-id-16)[.](https://www.npolar.no/en/quantarctica/#toggle-id-16)[no](https://www.npolar.no/en/quantarctica/#toggle-id-16)[/](https://www.npolar.no/en/quantarctica/#toggle-id-16)[en](https://www.npolar.no/en/quantarctica/#toggle-id-16)[/](https://www.npolar.no/en/quantarctica/#toggle-id-16)[quantarctica](https://www.npolar.no/en/quantarctica/#toggle-id-16)[/#](https://www.npolar.no/en/quantarctica/#toggle-id-16)[toggle](https://www.npolar.no/en/quantarctica/#toggle-id-16)[-](https://www.npolar.no/en/quantarctica/#toggle-id-16)[id](https://www.npolar.no/en/quantarctica/#toggle-id-16)[-16](https://www.npolar.no/en/quantarctica/#toggle-id-16)

1) File Name: **NSF\_ICF\_IceCoreMetadata.csv**

List of Ice cores from the US NSF ICF

[https](https://icecores.org/inventory)[://](https://icecores.org/inventory)[icecores](https://icecores.org/inventory)[.](https://icecores.org/inventory)[org](https://icecores.org/inventory)[/](https://icecores.org/inventory)[inventory](https://icecores.org/inventory)

Provided by Richard Nunn on August 15, 2022

Assistant Curator, NSF-Ice Core Facility

1) File Name: **icecores.db (**sqlite3 file format) database of ice cores from wikipedia was extracted into the following CSV tables listed below using DB Browser for SQLite Version 3.12.2 application  (https://sqlitebrowser.org/) : 

File Name: **citations.csv**

File Name: **citations\_sources.csv**

File Name: **icecores.csv**

File Name: **icecores\_citations.csv**

File Name: **icecores\_notes.csv**

File Name: **notes.csv**

File Name: **sources.csv**

This list of ice core sites was collected from wikipedia page on August 2, 2022

[https](https://en.wikipedia.org/wiki/List_of_ice_cores)[://](https://en.wikipedia.org/wiki/List_of_ice_cores)[en](https://en.wikipedia.org/wiki/List_of_ice_cores)[.](https://en.wikipedia.org/wiki/List_of_ice_cores)[wikipedia](https://en.wikipedia.org/wiki/List_of_ice_cores)[.](https://en.wikipedia.org/wiki/List_of_ice_cores)[org](https://en.wikipedia.org/wiki/List_of_ice_cores)[/](https://en.wikipedia.org/wiki/List_of_ice_cores)[wiki](https://en.wikipedia.org/wiki/List_of_ice_cores)[/](https://en.wikipedia.org/wiki/List_of_ice_cores)[List](https://en.wikipedia.org/wiki/List_of_ice_cores)[_](https://en.wikipedia.org/wiki/List_of_ice_cores)[of](https://en.wikipedia.org/wiki/List_of_ice_cores)[_](https://en.wikipedia.org/wiki/List_of_ice_cores)[ice](https://en.wikipedia.org/wiki/List_of_ice_cores)[_](https://en.wikipedia.org/wiki/List_of_ice_cores)[cores](https://en.wikipedia.org/wiki/List_of_ice_cores) into **icecores.db file** using Python 3 script **icecorescraper.py,** developed by Mark Royer [mark](mailto:mark.royer@maine.edu)[.](mailto:mark.royer@maine.edu)[royer](mailto:mark.royer@maine.edu)[@](mailto:mark.royer@maine.edu)[maine](mailto:mark.royer@maine.edu)[.](mailto:mark.royer@maine.edu)[edu](mailto:mark.royer@maine.edu)

[https](https://www.umpi.edu/campus-directory/mark-royer/)[://](https://www.umpi.edu/campus-directory/mark-royer/)[www](https://www.umpi.edu/campus-directory/mark-royer/)[.](https://www.umpi.edu/campus-directory/mark-royer/)[umpi](https://www.umpi.edu/campus-directory/mark-royer/)[.](https://www.umpi.edu/campus-directory/mark-royer/)[edu](https://www.umpi.edu/campus-directory/mark-royer/)[/](https://www.umpi.edu/campus-directory/mark-royer/)[campus](https://www.umpi.edu/campus-directory/mark-royer/)[-](https://www.umpi.edu/campus-directory/mark-royer/)[directory](https://www.umpi.edu/campus-directory/mark-royer/)[/](https://www.umpi.edu/campus-directory/mark-royer/)[mark](https://www.umpi.edu/campus-directory/mark-royer/)[-](https://www.umpi.edu/campus-directory/mark-royer/)[royer](https://www.umpi.edu/campus-directory/mark-royer/)[/](https://www.umpi.edu/campus-directory/mark-royer/)

Python script usage: ./icecorescraper.py -h

**Dependencies:**

File Name: **Icecores-schema.sql** 

**Beautiful Soup** python package (https://beautiful-soup-4.readthedocs.io/en/latest/#)

sqlite3  python module
## **Contact person for the data set** 
Andrei Kurbatov: [akurbatov](mailto:akurbatov@maine.edu)[@](mailto:akurbatov@maine.edu)[maine](mailto:akurbatov@maine.edu)[.](mailto:akurbatov@maine.edu)[edu](mailto:akurbatov@maine.edu)

https://orcid.org/0000-0002-9819-9251
