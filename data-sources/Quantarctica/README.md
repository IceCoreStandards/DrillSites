# Ice Cores Database

"Ice Core locations and metadata compiled from multiple sources for Quantarctica
 

IceReader: http://www.icereader.org/
Climate Change Institute Antarctic Ice Core Data: https://www.icecoredata.org/cci/Antarctica.html

WAIS Divide Project summary paper: https://www.nature.com/articles/nature12376"


## Complied in 2017 by Peter Neff
https://www.npolar.no/quantarctica/#toggle-id-16

## Links updated in 2022

