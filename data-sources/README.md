#  Collection of  ice core borehole source data
## Data set was  originally developed by participants of NSF Summer 2019 workshop: Computing Arctic Data: Orono, ME - Spring 2019
## Funded by the NSF grant 1848747


Inspired by:  [International Ocean Discovery Program](https://iodp.tamu.edu/scienceops/maps.html)

